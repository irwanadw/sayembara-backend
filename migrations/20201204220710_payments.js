// created by npm run migrate:make -- tables

exports.up = function (knex) {
    return knex.schema
        .createTable('payments', function (table) {
            table.uuid('id').primary()
            table.uuid('user_id').notNullable()
            table.foreign('user_id')
                .references('id').inTable('users')
            table.uuid('contest_id').notNullable()
            table.foreign('contest_id')
                .references('id').inTable('contests')
            table.datetime('payment_date').nullable()
            table.string('provider_payment_url', 255).nullable()
            table.boolean('payment_status').defaultTo(false)
        })
};

exports.down = function (knex) {
    knex.schema
        .dropTable('payments')
};
