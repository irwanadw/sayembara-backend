// created by npm run migrate:make -- tables

exports.up = function (knex) {
    return knex.schema
        .createTable('users', function (table) {
            table.uuid('id').primary()
            table.string('role', 24).notNullable()
            table.string('email', 68).notNullable()
            table.unique('email')
            table.string('password', 255).notNullable()
            table.datetime('created_at').notNullable().defaultTo(knex.fn.now())
            table.datetime('updated_at').notNullable().defaultTo(knex.fn.now())
        })
};

exports.down = function (knex) {
    knex.schema
        .dropTable('users')
};
