// created by npm run migrate:make -- tables

exports.up = function (knex) {
    return knex.schema
        .createTable('contests', function (table) {
            table.uuid('id').primary()
            table.uuid('user_id').notNullable()
            table.foreign('user_id')
                .references('id').inTable('users')
            table.string('winner_submission_id', 36).nullable()
            table.string('title', 50).notNullable()
            table.string('status', 20).notNullable().defaultTo('pending')
            table.integer('prize', 20).notNullable()
            table.string('description', 1000).notNullable()
            table.string('poster_url', 255).notNullable()
            table.string('poster_thumbnail_url', 255).notNullable()
            table.string('winner_payment_url', 255).notNullable()
            table.boolean('is_winner_paid').defaultTo(false)
            table.datetime('start_date').notNullable()
            table.datetime('end_date').notNullable()
            table.datetime('announcement_date').notNullable()
            table.datetime('post_time').notNullable()
            table.datetime('payment_winner_date').nullable()
        })
};

exports.down = function (knex) {
    knex.schema
        .dropTable('contests')
};
