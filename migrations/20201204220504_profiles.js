// created by npm run migrate:make -- tables

exports.up = function (knex) {
    return knex.schema
        .createTable('profiles', function (table) {
            table.uuid('id').primary()
            table.uuid('user_id').notNullable()
            table.foreign('user_id')
                .references('id').inTable('users')
            table.string('firstname', 50).nullable()
            table.string('lastname', 50).nullable()
            table.string('location', 255).nullable()
            table.string('bank', 50).nullable()
            table.string('company_name', 50).nullable()
            table.integer('account_number', 50).nullable()
        })
};

exports.down = function (knex) {
    knex.schema
        .dropTable('profiles')
};
