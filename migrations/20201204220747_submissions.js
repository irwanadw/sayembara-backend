// created by npm run migrate:make -- tables

exports.up = function (knex) {
    return knex.schema
        .createTable('submissions', function (table) {
            table.uuid('id').primary()
            table.uuid('user_id').notNullable()
            table.foreign('user_id')
                .references('id').inTable('users')
            table.uuid('contest_id').notNullable()
            table.foreign('contest_id')
                .references('id').inTable('contests')
            table.string('title').notNullable()
            table.string('submitted_poster_url', 255).nullable()
            table.string('description', 255).notNullable()
            table.datetime('submission_date').nullable()
        })
};

exports.down = function (knex) {
    knex.schema
        .dropTable('submissions')
};
