const { cloneDeep } = require("lodash")

class ErrorHelper {
  constructor(code, message, detail, stack) {
    this.code = code
    this.message = message
    this.detail = detail
    this.stack = stack
  }
  hander(err, req, res, next) {
    if (!isNaN(err.code))
      res.status(err.code).send(err)
    else if (err.code == 'ER_NO_REFERENCED_ROW_2')
      res.status(400).send({
        code: 400,
        message: "NO_REFERENCED",
        detail: "referenced data is not available"
      })
    else {
      console.error(err);
      res.status(500).send({
        code: 500,
        message: "SERVER_ERROR",
        detail: err.code,
        stack: cloneDeep(err) // express can't send error object, should convert to plain first
      })
    }
  }
}

module.exports = ErrorHelper