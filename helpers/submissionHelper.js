const { isEmpty } = require("lodash")
const Controller = require('../controller/dbController')
const submission = new Controller('submissions')

async function validationSubmit(result, query, req, res) {
    let body = {};
    const userSubmission = await submission.get({
        user_id: req.user.id,
        contest_id: req.query.contest_id
    })
    var isColumnEmpty = [
        userSubmission[0].firstSubmittedPosterUrl,
        userSubmission[0].secondSubmittedPosterUrl,
        userSubmission[0].thirdSubmittedPosterUrl
    ]
    if (result.length == 1) {

        if (isEmpty(isColumnEmpty[0])) {
            body.first_submitted_poster_url = await result[0]
            await submission.uploadSubmissions(query, body)
            return res.status(200).json({ Uploaded: body })
        } else if (isEmpty(isColumnEmpty[1])) {
            body.second_submitted_poster_url = await result[0]
            await submission.uploadSubmissions(query, body)
            return res.status(200).json({ Uploaded: body })
        } else {
            body.third_submitted_poster_url = await result[0]
            await submission.uploadSubmissions(query, body)
            return res.status(200).json({ Uploaded: body })
        }

    } else if (result.length == 2) {
        if (isEmpty(isColumnEmpty[0]) && isEmpty(isColumnEmpty[1])) {
            body.first_submitted_poster_url = await result[0]
            body.second_submitted_poster_url = await result[1]
            await submission.uploadSubmissions(query, body)
            return res.status(200).json({ Uploaded: body })
        } else if (isEmpty(isColumnEmpty[1]) && isEmpty(isColumnEmpty[2])) {
            body.second_submitted_poster_url = await result[1]
            body.third_submitted_poster_url = await result[2]
            await submission.uploadSubmissions(query, body)
            return res.status(200).json({ Uploaded: body })
        } else if (isEmpty(isColumnEmpty[0]) || isEmpty(isColumnEmpty[1]) || isEmpty(isColumnEmpty[2])) {
            return res.status(401).json({ err: `pick one file to upload` })
        }

    } else if (result.length == 3) {
        body.first_submitted_poster_url = await result[0]
        body.second_submitted_poster_url = await result[1]
        body.third_submitted_poster_url = await result[2]
        await submission.uploadSubmissions(query, body)
        return res.status(200).json({ Uploaded: body })
    }
}

async function checkingColumnAvailability(req, res, files) {
    const userSubmission = await submission.get({
        user_id: req.user.id,
        contest_id: req.query.contest_id
    })
    var isColumnEmpty = [
        userSubmission[0].firstSubmittedPosterUrl,
        userSubmission[0].secondSubmittedPosterUrl,
        userSubmission[0].thirdSubmittedPosterUrl
    ]
    if (userSubmission.length == 0) {
        res.status(404).json({ err: `can't find the data` })
        return 1

    }
    if (userSubmission[0].firstSubmittedPosterUrl && userSubmission[0].secondSubmittedPosterUrl && userSubmission[0].thirdSubmittedPosterUrl) {
        res.status(401).json({ err: ` upload limit ` })
        return 2
    }
    if (files.length == 2) {
        if (!(isEmpty(isColumnEmpty[1]))) {
            res.status(401).json({ err: `pick one file to upload` })
            return 3
        }
    }

}

module.exports = {
    validationSubmit,
    checkingColumnAvailability
}