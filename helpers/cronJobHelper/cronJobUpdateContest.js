const cronJobController = require('../../controller/cronJobController');

const CronJob = require('cron').CronJob;
const controller = new cronJobController('contests')

//Job set at 01.00 every day
const jobUpdateContest = new CronJob('00 01 * * *', async function () {
    //update contest that have passed time limit
    const result = await controller.editStatusContest({ status: "close" })
        .catch((err) => {
            console.log(err);
        })
    console.log(`${result} record has updated`);
}, null, true, 'Asia/Jakarta');

module.exports = jobUpdateContest