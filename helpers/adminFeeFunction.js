function getAdminFee(prize) {
    if (prize >= 500000 && prize <= 1000000) return 50000
    else if (prize > 1000000 && prize <= 3000000) return 100000
    else if (prize > 3000000 && prize <= 5000000) return 200000
    else if (prize > 5000000 && prize <= 7000000) return 300000
    else if (prize > 7000000 && prize < 10000000) return 400000
    else if (prize >= 10000000) return 500000
    else return 0
}

module.exports = getAdminFee