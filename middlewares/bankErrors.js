module.exports = {
    ER_BAD_FIELD_ERROR: {
        status: 400,
        respon: { err: "Bad request!" }
    },
    ER_DUP_ENTRY: {
        status: 409,
        respon: { err: "Username or email already exist!" }
    },
    ER_NO_DEFAULT_FOR_FIELD: {
        status: 400,
        respon: { err: "Bad request!" }
    },
    ER_PARSE_ERROR: {
        status: 400,
        respon: { err: "wrong structure value" }
    },
    ER_SP_UNDECLARED_VAR: {
        status: 400,
        respon: { err: "Wrong SQL Syntax!" }
    },
    ER_DUP_ENTRY: {
        status: 404,
        respon: { err: "Duplicate Entry" }
    },
    ER_DATA_TOO_LONG: {
        status: 400,
        respon: { err: "Request too long!" }
    },
    ERR_HTTP_HEADERS_SENT: {
        status: 400,
        respon: { err: "Request not complete" }
    },
    ER_NO_DEFAULT_FOR_FIELD: {
        status: 400,
        respon: { err: "Request not complete" }
    }
}