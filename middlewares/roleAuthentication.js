module.exports = function roleAuthentication(role) {
    return ((req, res, next) => {
        if (role.includes(req.user.role)) {
            next()
        } else {
            res.status(404).json({ err: `you are ${req.user.role}, can't access this route` })
        }
    })
}