const _ = require('lodash')
const bankErrors = require('./bankErrors')

function routeErrorHandler(err, req, res, next) {
    const bankErrorsKeys = Object.keys(bankErrors)
    const parsedError = _.toPlainObject(err)
    if (bankErrorsKeys.includes(parsedError.code)) {
        const error = bankErrors[parsedError.code]
        res.status(error.status).send(error.respon)
    } else {
        res.status(500).json({ error: err })
    }

}

module.exports = { routeErrorHandler }
