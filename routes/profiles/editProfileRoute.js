const express = require('express')
const Controller = require('../../controller/dbController')
const app = express.Router()


app.patch('/test', async (req, res) => {
    const query = req.body
    const data = new Controller('contests')

    try {
        await data.edit(query.id, query)
        res.send('edited')
    } catch (err) {
        throw err
    }
})

module.exports = app