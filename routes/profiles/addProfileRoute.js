const express = require('express')
const Controller = require('../../controller/dbController')
const app = express.Router()


app.post('/profile', async (req, res) => {
    const query = req.body
    const data = new Controller('profile')

    try {
        await data.add(query)
        res.send('data has been added')
    } catch (err) {
        throw err
    }

})

module.exports = app