const express = require('express')
const Controller = require('../../controller/dbController')
const app = express.Router()

app.get('/test', async (req, res) => {
    const query = req.body
    const data = new Controller('contests')

    try {
        res.send(await data.get(query))
    } catch (err) {
        throw err
    }

})


module.exports = app