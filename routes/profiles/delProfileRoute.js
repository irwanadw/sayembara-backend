const express = require('express')
const Controller = require('../../controller/dbController')
const app = express.Router()

app.delete('/test', async (req, res) => {
    const query = req.body
    const data = new Controller('contests')

    try {
        await data.remove(query.id)
        res.send('deleted')
    } catch (err) {
        throw err
    }

})

module.exports = app