const express = require('express')
const app = express.Router()
const multer = require('multer')
const path = require('path')
const ImageKit = require("imagekit");
const Controller = require('../../../controller/dbController');
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware');
const passport = require('passport');
const roleAuthentication = require('../../../middlewares/roleAuthentication');
const { IMAGEKIT_PUBLIC, IMAGEKIT_PRIVATE, IMAGEKIT_ENDPOINT } = process.env

const imagekit = new ImageKit({
    publicKey: IMAGEKIT_PUBLIC,
    privateKey: IMAGEKIT_PRIVATE,
    urlEndpoint: IMAGEKIT_ENDPOINT
});
let ext
let filter
const storage = multer.memoryStorage();
const upload = multer({
    storage: storage,
    limits: { fileSize: 10000000 }, // 10 MB
    fileFilter: function (req, file, callback) {
        const imgExt = ['.png', '.jpg', '.jpeg']
        ext = path.extname(file.originalname);
        if (!imgExt.includes(ext)) {
            filter = 'Wrong ext'
        }
        callback(null, true)
    }
});

app.post('/provider/payment', passport.authenticate('bearer', { session: false }), roleAuthentication(['provider']), upload.single('file'), async (req, res, next) => {
    // this is to upload file into Imagekit.io
    const contestId = req.query.contestId
    const controller = new Controller('payments')
    const contestController = new Controller('contests')
    const userId = req.user.id
    if (filter === 'Wrong ext') {
        res.status(400).send("Wrong file extention")
        return
    }
    try {
        const payment = await controller.get({ contestId })
        if (payment[0].userId != userId) {
            res.status(400).send("Unauthorized")
            return
        }
        if (!payment.length) {
            res.status(404).send("Contest id not found!")
            return
        }
        if (payment[0].paymentStatus == true) {
            res.status(400).send("Payment has been done")
            return
        }
        imagekit.upload({
            file: req.file.buffer,
            fileName: `${contestId}${ext}`,
            folder: "contest_payment"
        }).then(async (response) => {
            const updatePayment = {
                paymentDate: new Date(),
                providerPaymentUrl: response.url,
                payment_status: true
            }
            const update = await controller.edit(payment[0].id, updatePayment)
            if (update) {
                await contestController.edit(contestId, { status: "payment review" })
            }
        })
        res.send({ status: 'Data send!' })
    } catch (error) {
        next(error)
    }

})

app.use(routeErrorHandler)
module.exports = app 
