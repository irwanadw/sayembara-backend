const express = require('express')
const app = express.Router()
const multer = require('multer')
const ImageKit = require("imagekit");
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware');
const watermark = require('../../../controller/watermark');
const { IMAGEKIT_PUBLIC, IMAGEKIT_PRIVATE, IMAGEKIT_ENDPOINT, URL_LOGO } = process.env
const passport = require('../../../middlewares/authorizationMiddleware')
const roleAuthentication = require('../../../middlewares/roleAuthentication');
const { validationSubmit, checkingColumnAvailability } = require('../../../helpers/submissionHelper');

// imagekit configuration
const imagekit = new ImageKit({
    publicKey: IMAGEKIT_PUBLIC,
    privateKey: IMAGEKIT_PRIVATE,
    urlEndpoint: IMAGEKIT_ENDPOINT
});
// multer configuration
const storage = multer.memoryStorage();
const upload = multer({
    storage: storage,
    limits: { fileSize: 1000000 } // 1 MB
});
app.patch('/participant/submissions/upload',
    passport.authenticate('bearer', { session: false }), roleAuthentication(['participant']),
    upload.any('file'), express.static('uploads'), async (req, res, next) => {
        const files = req.files
        const allowedType = ["image/jpeg", "image/png", "image/jpg"]
        let query = req.query
        let type;
        query.user_id = req.user.id

        const checking = await checkingColumnAvailability(req, res, files)
        if (checking == 1)
            return
        if (checking == 2)
            return
        if (checking == 3)
            return

        let urlFiles = Promise.all(
            files.map(async (file, index) => {
                type = file.mimetype
                if (!(allowedType.includes(type)))
                    return res.status(401).json({ err: 'only PNG, JPG, JPEG allowed' })

                // jimp configuration
                const ORIGINAL_IMAGE = file.buffer;
                const LOGO = URL_LOGO
                var bit = await watermark(ORIGINAL_IMAGE, LOGO, type)
                    .then(async image => {
                        return image
                    })

                return await imagekit.upload({
                    file: bit,
                    fileName: `.${type.split('/')[1]}`,
                    folder: "Submission_Contests"
                })
                    .then(async response => {
                        return await response.url
                    })
                    .catch(error => {
                        next(error);
                    })
            })
        )

        urlFiles.then(async result => {

            try {
                return await validationSubmit(result, query, req, res)
            } catch (err) {
                next(err)
            }

        })
    })

app.use(routeErrorHandler)
module.exports = app