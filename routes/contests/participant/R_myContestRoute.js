const express = require('express')
const app = express.Router()
const passport = require('../../../middlewares/authorizationMiddleware')
const Controller = require('../../../controller/dbController')
const roleAuthentication = require('../../../middlewares/roleAuthentication')
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware')

app.get('/participant/submissions', passport.authenticate('bearer', { session: false }), roleAuthentication(['participant']), async (req, res, next) => {
    const submission = new Controller('submissions')
    const offset = (req.query.page - 1) * req.query.limit
    let query = req.query

    try {
        // GET all contests followed by this user
        if (query.status == undefined && query.slug == undefined) {
            const result = await submission.getMyContestSubmission({ "submissions.user_id": req.user.id },
                offset, req.query.limit)
            return res.send(result)
        }
        // FIND all contests followed by this user using .....
        if (query.status && query.slug == undefined) {
            const result = await submission.getMyContestSubmission({
                "submissions.user_id": req.user.id,
                "contests.status": query.status
            }, offset, req.query.limit)
            if (result.length == 0)
                return res.status(404).json({ err: `can't find the data` })
            else
                return res.status(200).json(result)
        }
        if (query.slug && query.status == undefined) {
            const result = await submission.getMyContestSubmission({
                "submissions.user_id": req.user.id,
                "contests.slug": query.slug
            }, offset, req.query.limit)
            if (result.length == 0)
                return res.status(404).json({ err: `can't find the data` })
            else
                return res.status(200).json(result)
        }
        if (query.slug && query.status) {
            const result = await submission.getMyContestSubmission({
                "submissions.user_id": req.user.id,
                "contests.slug": query.slug,
                "contests.status": query.status
            }, offset, req.query.limit)
            if (result.length == 0)
                return res.status(404).json({ err: `can't find the data` })
            else
                return res.status(200).json(result)
        }
    } catch (error) {
        next(error)
    }

})
app.use(routeErrorHandler)
module.exports = app