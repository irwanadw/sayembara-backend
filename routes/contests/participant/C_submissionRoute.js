const express = require('express')
const app = express.Router()
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware')
const Controller = require('../../../controller/dbController')
const passport = require('../../../middlewares/authorizationMiddleware')
const roleAuthentication = require('../../../middlewares/roleAuthentication')

app.post('/participant/submissions', passport.authenticate('bearer', { session: false }), roleAuthentication(['participant']), async (req, res, next) => {
    const contest_id = req.body.contest_id
    const body = req.body
    body.user_id = req.user.id

    const controller = new Controller('submissions')
    const contest = new Controller('contests')
    const isClosed = await contest.get({ id: contest_id })
    const haveSubmitted = await controller.get({ contest_id: contest_id, user_id: req.user.id })
    if (haveSubmitted.length != 0)
        return res.status(401).json({ err: `you have registered this contest already` })
    if (isClosed[0].status != 'open') {
        return res.status(401).json({ err: `contest not opened` })
    }

    try {
        await controller.add(body)
        return res.status(200).json(body)
    } catch (error) {
        next(error)
    }

})

app.use(routeErrorHandler)
module.exports = app