const express = require('express')
const app = express.Router()
const passport = require('../../../middlewares/authorizationMiddleware')
const Controller = require('../../../controller/dbController')
const roleAuthentication = require('../../../middlewares/roleAuthentication')
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware')

app.get('/participant/submissions/detail', passport.authenticate('bearer', { session: false }), roleAuthentication(['participant', 'provider']), async (req, res, next) => {
    const submission = new Controller('submissions')

    let query = req.query

    const offset = (req.query.page - 1) * req.query.limit
    const limit = req.query.limit

    try {

        const result = await submission.submissionDetail({ "contests.id": query.id }, limit, offset)
        if (result.length == 0)
            return res.status(404).json({ err: `can't find the data by the ID` })
        else
            return res.status(200).json(result)
    } catch (error) {
        next(error)
    }

})
app.use(routeErrorHandler)
module.exports = app