const express = require('express')
const Controller = require('../../../controller/dbController')
const passport = require('../../../middlewares/authorizationMiddleware')
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware')
const roleAuthentication = require('../../../middlewares/roleAuthentication')
const Contest = require('../../../model/modelContest')
const app = express.Router()

app.post('/contests', passport.authenticate('bearer', { session: false }), roleAuthentication(['provider']), async (req, res, next) => {
    const body = req.body
    const user = req.user
    body.userId = user.id
    const contests = new Contest(body)
    const controller = new Controller('payments')
    try {
        contests.validate()
        contests.validateDate()
        const result = await contests.save()
        const payment = {
            userId: user.id,
            contestId: result.id
        }
        await controller.add(payment)
        res.send(result)
    } catch (error) {
        next(error)
    }
})
app.use(routeErrorHandler)
module.exports = app