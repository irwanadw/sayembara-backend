const express = require('express')
const paginate = require('express-paginate')
const paginateController = require('../../../controller/paginateController')
const getAdminFee = require('../../../helpers/adminFeeFunction')
const passport = require('../../../middlewares/authorizationMiddleware')
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware')
const roleAuthentication = require('../../../middlewares/roleAuthentication')
const app = express.Router()

app.get('/provider/contests', paginate.middleware(), passport.authenticate('bearer', { session: false }), roleAuthentication(['provider']), async (req, res, next) => {
    const userId = req.user.id
    const page = req.query.page
    const limit = req.query.limit
    const offset = (page - 1) * limit
    const query = req.query

    query.userId = userId
    delete query.limit
    delete query.page

    let title = " "
    if (query.title) {
        title = query.title
        delete query.title
    }

    const controller = new paginateController('contests', 'profiles', 'categories')
    try {
        const result = await controller.getPaginateProviderContest(query, title, limit, offset)
        result.map((element) => {
            element.adminFee = getAdminFee(element.prize)
            element.totalPayment = element.prize + element.adminFee
        })
        const itemCount = await controller.getCount(query, title)
        if (query.id) {
            res.send(result)
            return
        }
        const totalCount = itemCount[0].count
        const pageCount = Math.ceil(totalCount / limit);
        delete query.userId
        query.page = page
        query.limit = limit
        const response = {
            contests: result,
            pageCount,
            itemCount,
            pages: paginate.getArrayPages(req)(limit, pageCount, page)
        }
        res.send(response);
    } catch (error) {
        next(error)
    }
})
app.use(routeErrorHandler)
module.exports = app