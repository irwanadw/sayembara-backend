const express = require('express')
const app = express.Router()
const multer = require('multer')
const path = require('path')
const ImageKit = require("imagekit");
const passport = require('passport');
const roleAuthentication = require('../../../middlewares/roleAuthentication');
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware');
const Controller = require('../../../controller/dbController');
const { IMAGEKIT_PUBLIC, IMAGEKIT_PRIVATE, IMAGEKIT_ENDPOINT } = process.env

const imagekit = new ImageKit({
    publicKey: IMAGEKIT_PUBLIC,
    privateKey: IMAGEKIT_PRIVATE,
    urlEndpoint: IMAGEKIT_ENDPOINT
});

let ext
let filter
const storage = multer.memoryStorage();
const upload = multer({
    storage: storage,
    limits: { fileSize: 1000000000 }, // 10 MB
    fileFilter: function (req, file, callback) {
        const imgExt = ['.png', '.jpg', '.jpeg']
        ext = path.extname(file.originalname);
        if (!imgExt.includes(ext)) {
            filter = 'Wrong ext'
        }
        callback(null, true)
    }
});
app.post('/contests/upload', passport.authenticate('bearer', { session: false }), roleAuthentication(['provider']), upload.single('poster'), async (req, res, next) => {
    // this is to upload file into Imagekit.io
    const contestId = req.query.contestId
    const contestController = new Controller('contests')
    const userId = req.user.id
    if (filter === 'Wrong ext') {
        res.status(400).send("Wrong file extention")
        return
    }
    try {
        const contests = await contestController.get({ id: contestId })
        if (contests[0].userId != userId) {
            res.status(400).send("Unauthorized")
            return
        }
        if (!contests.length) {
            res.status(404).send("Contest id not found!")
            return
        }
        imagekit.upload({
            file: req.file.buffer,
            fileName: `${contestId}.${ext}`,
            folder: "contest_poster"
        }).then(async (response) => {
            const updateContest = {
                posterUrl: response.url,
                posterThumbnailUrl: response.thumbnailUrl
            }
            await contestController.edit(contestId, updateContest)
            res.send({ status: "Data Saved" })
        })

    } catch (error) {
        res.send(error)
    }

})

app.use(routeErrorHandler)
module.exports = app 
