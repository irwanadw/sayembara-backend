const express = require('express')
const Controller = require('../../../controller/dbController')
const passport = require('../../../middlewares/authorizationMiddleware')
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware')
const roleAuthentication = require('../../../middlewares/roleAuthentication')
const app = express.Router()

app.post('/provider/contests/winner', passport.authenticate('bearer', { session: false }), roleAuthentication(['provider']), async (req, res, next) => {
    const body = req.body
    const user = req.user
    const contest = new Controller('contests')
    const query = contest.camelKey(body)
    const isWinnerPaid = await contest.get({ id: body.id })
    const status = isWinnerPaid[0].status
    const isAnyWinner = isWinnerPaid[0].winner_id

    try {
        if (status === 'close' && isAnyWinner == null) {
            await contest.edit(query.id, { winner_subsmission_id: query.winner_subsmission_id })
            res.send(`Congratulation to ${query.winner_subsmission_id}`)
        } else if (user.id != isWinnerPaid[0].userId) {
            res.status(401).send('Unautorized')
        } else {
            res.send('contest must be closed first or winner is exist')
        }
    } catch (err) {
        next(err)
    }
})

app.use(routeErrorHandler)
module.exports = app