const express = require('express')
const passport = require('../../../middlewares/authorizationMiddleware')
const Controller = require('../../../controller/dbController')
const roleAuthentication = require('../../../middlewares/roleAuthentication')
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware')
const app = express.Router()

app.get('/admin/contests/winner', passport.authenticate('bearer', { session: false }), roleAuthentication(['admin']), async (req, res, next) => {
    const id = req.query.contest_id

    try {
        const data = new Controller('submissions')
        const result = await data.winnerBank(id)
        if (result.length == 0)
            return res.status(404).json({ err: `wrong contest ID ?` })
        return res.status(200).json(result[0])
    } catch (err) {
        next(err)
    }
})

app.use(routeErrorHandler)
module.exports = app