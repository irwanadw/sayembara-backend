const express = require('express')
const passport = require('../../../middlewares/authorizationMiddleware')
const Controller = require('../../../controller/dbController')
const roleAuthentication = require('../../../middlewares/roleAuthentication')
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware')
const app = express.Router()

app.get('/admin/contests/payment', passport.authenticate('bearer', { session: false }), roleAuthentication(['admin']), async (req, res, next) => {
    const id = req.query.contest_id
    const data = new Controller('contests')
    try {
        const contest = await data.getContestPayment({ "contests.id": id })
        if (!contest[0])
            return res.status(401).json({ err: `can't find the ID contests` })
        else
            return res.status(200).json(contest[0])
    } catch (err) {
        next(err)
    }
})

app.use(routeErrorHandler)
module.exports = app