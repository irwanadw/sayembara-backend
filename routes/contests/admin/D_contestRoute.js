const express = require('express')
const passport = require('../../../middlewares/authorizationMiddleware')
const Controller = require('../../../controller/dbController')
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware')
const app = express.Router()

app.delete('/admin/contests', async (req, res, next) => {
    // const body = req.body
    // const data = new Controller('contests')
    // try {
    //     res.send(await data.delete(body))
    // } catch (err) {
    //     next(err)
    // }
})

app.use(routeErrorHandler)
module.exports = app