const express = require('express')
const passport = require('../../../middlewares/authorizationMiddleware')
const Controller = require('../../../controller/dbController')
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware')
const roleAuthentication = require('../../../middlewares/roleAuthentication')
const app = express.Router()

app.post('/admin/contests', passport.authenticate('bearer', { session: false }), roleAuthentication(['admin']), async (req, res, next) => {
    const body = req.body
    const contest = new Controller('contests')
    const query = contest.camelKey(body)
    const isWinnerPaid = await contest.get({ id: body.id })
    const status = isWinnerPaid[0].status
    const isAnyWinner = isWinnerPaid[0].winner_id

    try {
        if (status === 'close' && isAnyWinner == null) {
            await contest.edit(query.id, { winner_id: query.winner_id })
            res.send(`Congratulation to ${query.winner_id}`)
        } else {
            res.send('contest must be closed first or winner is exist')
        }
    } catch (err) {
        next(err)
    }
})

app.use(routeErrorHandler)
module.exports = app