require('dotenv').config()
const express = require('express')
const passport = require('../../../middlewares/authorizationMiddleware')
const Controller = require('../../../controller/dbController')
const roleAuthentication = require('../../../middlewares/roleAuthentication')
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware')
const app = express.Router()
const { KEY_MAILGUN, DOMAIN_MAILGUN } = process.env

// mailgun configuration
const api_key = KEY_MAILGUN;
const domain = DOMAIN_MAILGUN;
const mailgun = require('mailgun-js')
const mg = mailgun({ apiKey: api_key, domain: domain });

app.patch('/admin/contests/reject', passport.authenticate('bearer', { session: false }), roleAuthentication(['admin']), async (req, res, next) => {
    const body = req.query

    const contest = new Controller('contests')
    const payment = new Controller('payments')
    const decamel = payment.camelKey(body)
    const paymentStatus = await payment.get({ contest_id: decamel.contest_id })
    const emailProvider = await payment.getEmail(body)

    const isWinnerExist = await contest.get({ id: decamel.contest_id })

    try {
        const statusContest = await contest.get({ id: decamel.contest_id })
        if (!(statusContest[0]))
            return res.status(404).json({ err: `wrong contests ID` })
        // mailgun configuration--------
        const data = {
            from: 'sayembara.projek@gmail.com', // admin email
            to: `${emailProvider[0].email}`, // provider email
            // to: "natannegara@gmail.com", // default email
            subject: `Payment Confirmation - Sayembara`,
            text: `Sorry! Your payment has been rejected by Admin.
            Please contact Admin`
        };
        // ------------------------------
        if ((statusContest[0].status) === 'open') {
            return res.status(401).json({ err: 'You have confirmed the payment of this contest' })
        } else {
            if (!paymentStatus[0]) {
                res.status(404).json({ err: 'no data payment' })
            } else if (paymentStatus[0].payment_status === 1) {
                if (isWinnerExist[0].winner_name) {
                    return res.status(401).json({ err: `contest finished, cant open` })
                }
                // payment status paid & awaiting for payment or true & false
                await contest.edit(decamel.contest_id, { status: "rejected" })
                // sending email regarding payment
                mg.messages().send(data, (error, body) => { next(error) });
                // response by server
                return res.status(200).json({ status: "rejected" })
            } else {
                return res.status(401).json({ err: `provider hasn't paid` })
            }
        }
    } catch (err) {
        next(err)
    }
})

app.use(routeErrorHandler)
module.exports = app