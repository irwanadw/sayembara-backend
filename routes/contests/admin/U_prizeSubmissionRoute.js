const express = require('express')
const passport = require('../../../middlewares/authorizationMiddleware')
const Controller = require('../../../controller/dbController')
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware')
const roleAuthentication = require('../../../middlewares/roleAuthentication')
const { isNull, isDate } = require('lodash')
const app = express.Router()

app.patch('/admin/contests/prize', passport.authenticate('bearer', { session: false }), roleAuthentication(['admin']), async (req, res, next) => {
    const body = req.query
    const contest = new Controller('contests')

    try {
        const info = await contest.get(body)
        if (!info[0])
            return res.status(404).json({ err: `can't find this ${body.id}` })
        if (info[0].is_winner_paid === 0 && isNull(info[0].payment_winner_date) && !(isNull(info[0].winner_name))) {
            if (info[0].status == 'open') {
                return res.status(401).json({ err: "contest is open, cant submit prize" })
            }
            await contest.edit(body.id, { is_winner_paid: 1, payment_winner_date: new Date() })
            return res.status(200).json({ success: 'The Prize has been delivered to the Winner' })
        } else {
            return res.status(404).json({ err: 'prize has sent or has no winner name' })
        }
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)
module.exports = app