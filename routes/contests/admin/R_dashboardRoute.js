const express = require('express')
const passport = require('../../../middlewares/authorizationMiddleware')
const Controller = require('../../../controller/dbController')
const roleAuthentication = require('../../../middlewares/roleAuthentication')
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware')
const app = express.Router()

app.get('/admin/contests', passport.authenticate('bearer', { session: false }), roleAuthentication(['admin']), async (req, res, next) => {
    const query = req.query
    const offset = (query.page - 1) * query.limit

    const data = new Controller('contests')
    try {
        let dashboard = await data.getContestProfile(query.limit, offset)
        var dt = new Date((dashboard[0].post_time));
        dt.setDate(+3);
        dashboard[0].provider_payment_date = dt
        delete dashboard[0].post_time
        return res.send(dashboard)
    } catch (err) {
        next(err)
    }
})

app.use(routeErrorHandler)
module.exports = app