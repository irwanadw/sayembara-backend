const express = require('express')
const app = express.Router()
const multer = require('multer')
const ImageKit = require("imagekit");
const passport = require('../../../middlewares/authorizationMiddleware')
const { routeErrorHandler } = require('../../../middlewares/errorMiddleware');
const roleAuthentication = require('../../../middlewares/roleAuthentication');
const Controller = require('../../../controller/dbController');
const { IMAGEKIT_PUBLIC, IMAGEKIT_PRIVATE, IMAGEKIT_ENDPOINT } = process.env

const imagekit = new ImageKit({
    publicKey: IMAGEKIT_PUBLIC,
    privateKey: IMAGEKIT_PRIVATE,
    urlEndpoint: IMAGEKIT_ENDPOINT
});

const storage = multer.memoryStorage();
const upload = multer({
    storage: storage,
    limits: { fileSize: 1000000 } // 1 MB
});

app.post('/admin/payment/file', passport.authenticate('bearer', { session: false }),
    roleAuthentication(['admin']), upload.single('file'), async (req, res, next) => {
        const contest = new Controller('contests')
        const id = req.query.id
        const result = await contest.get({ id: id })
        if (!result[0])
            return res.status(404).json({ err: 'cant find the data' })
        // this is to upload file into Imagekit.io
        imagekit.upload({
            file: req.file.buffer,
            fileName: "payment.jpg",
            folder: "payment_to_winner"
        }).then(async (response) => {
            await contest.edit(id, { winner_payment_url: response.url })
            return res.status(200).json({ message: `invoice uploaded succesfuly ${response.url}` });
        }).catch(error => {
            next(error);
        });
    })

app.use(routeErrorHandler)
module.exports = app