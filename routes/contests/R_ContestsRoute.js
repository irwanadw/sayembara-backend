const express = require('express')
const paginate = require('express-paginate')
const paginateController = require('../../controller/paginateController')
const { routeErrorHandler } = require('../../middlewares/errorMiddleware')
const app = express.Router()

app.get('/contests', paginate.middleware(), async (req, res, next) => {
    const page = req.query.page
    const limit = req.query.limit
    const offset = (page - 1) * limit
    const query = req.query
    delete query.limit
    delete query.page

    let title = " "
    if (query.title) {
        title = query.title
        delete query.title
    }

    const controller = new paginateController('contests', 'profiles', 'categories')

    try {
        const result = await controller.getPaginateContest(query, title, limit, offset)
        if (query.id) {
            res.send(result)
            return
        }
        const itemCount = await controller.getCountContest(query, title)
        const totalCount = itemCount[0].count
        const pageCount = Math.ceil(totalCount / limit);
        query.limit = limit
        query.page = page
        const response = {
            contests: result,
            pageCount,
            itemCount,
            pages: paginate.getArrayPages(req)(limit, pageCount, page)
        }
        res.send(response);
    } catch (error) {
        next(error)
    }
})
app.use(routeErrorHandler)
module.exports = app