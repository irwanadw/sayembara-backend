const express = require('express')
const { routeErrorHandler } = require('../../middlewares/errorMiddleware')
const User = require('../../model/userModel')
const app = express.Router()

app.post('/login', async (req, res, next) => {
    const body = req.body
    const user = new User(body)

    if (await user.structureBody() == false)
        return res.status(401).json({ err: "wrong format" })
    try {
        if (await user.login() == false)
            return res.status(404).json({ err: `user doesn't exist` })
        if (await user.checkPassword() == false)
            return res.status(404).json({ err: `password doesn't match` })
        user.signJwt()
        res.send(user.body)
    } catch (error) {
        next(error)
    }
})
app.use(routeErrorHandler)
module.exports = app
