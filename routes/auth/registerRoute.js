const express = require('express')
const Controller = require('../../controller/dbController')
const { routeErrorHandler } = require('../../middlewares/errorMiddleware')
const User = require('../../model/userModel')
const app = express.Router()

app.post('/register', async (req, res, next) => {
    let body = req.body
    let firstname = body.firstname
    let lastname = body.lastname
    let company_name = body.company_name
    const user = new User(body)
    const profile = new Controller('profiles')
    try {
        // validating the value structure of username and password
        user.validate()
        // salt the password and running POST request to login (add controller)
        await user.hashingAndRegister()
        // authenticating password and username using JWT
        let newUser = user.signJwt()
        if (newUser.role === 'participant') {
            await profile.add({
                user_id: newUser.id,
                firstname: firstname, lastname: lastname
            })
            body.firstname = firstname
            body.lastname = lastname
        }
        if (newUser.role === 'provider') {
            await profile.add({
                user_id: newUser.id,
                company_name: company_name
            })
            body.company_name = company_name
        }
        // if success, body will be sent
        res.send(user.body)
    } catch (error) {
        next(error)
    }
})
app.use(routeErrorHandler)
module.exports = app