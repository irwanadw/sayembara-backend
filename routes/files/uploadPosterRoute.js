const express = require('express')
const app = express.Router()
const multer = require('multer')
const ImageKit = require("imagekit");
const Jimp = require("jimp");
const { routeErrorHandler } = require('../../middlewares/errorMiddleware');
const { IMAGEKIT_PUBLIC, IMAGEKIT_PRIVATE, IMAGEKIT_ENDPOINT } = process.env

// imagekit configuration
const imagekit = new ImageKit({
    publicKey: IMAGEKIT_PUBLIC,
    privateKey: IMAGEKIT_PRIVATE,
    urlEndpoint: IMAGEKIT_ENDPOINT
});
// multer configuration
const storage = multer.memoryStorage();
const upload = multer({
    storage: storage,
    limits: { fileSize: 1000000 } // 1 MB
});

app.get('/file', upload.single('file'), express.static('uploads'), async (req, res, next) => {
    var a = req.file.originalname
    var b = a.split('').reverse().join('')
    if (b.slice(0, 4) != 'gnp.' || b.slice(0, 4) != 'gpj.')
        return res.send('wrong format')
    // jimp configuration
    const ORIGINAL_IMAGE = req.file.buffer;
    const LOGO = "https://ik.imagekit.io/2jhxitql4zf/IMGBIN_paper-watermark-postage-stamps-business-png_pU3w0JWW_pwQhqckwLJDj.png"
    const [image, logo] = await Promise.all([
        Jimp.read(ORIGINAL_IMAGE),
        Jimp.read(LOGO)
    ]);

    // logo.resize(Jimp.AUTO, image.bitmap.height / 1.5);
    logo.resize(image.bitmap.width / 2, Jimp.AUTO);

    const X = image.bitmap.width / 5;
    const Y = image.bitmap.height / 5;

    image
        .composite(logo, X, Y, [
            {
                mode: Jimp.BLEND_SCREEN,
                opacitySource: 0.5,
                opacityDest: 0.5
            }
        ])
        .getBuffer(Jimp.MIME_JPEG, (err, buffer) => {
            // res.set('Content-Type', Jimp.MIME_JPEG)
            // return res.send(buffer)
            imagekit.upload({
                file: buffer,
                fileName: "payment.jpg",
                folder: "SAYEMBARA_Payment"
            }).then(response => {
                // res.set('Content-Type', Jimp.MIME_JPEG)
                res.send(response)
            }).catch(error => {
                next(error);
            })
        })
})

app.use(routeErrorHandler)
module.exports = app