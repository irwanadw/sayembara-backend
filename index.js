const app = require('./server');

// Start the server

app.listen(process.env.PORT, () => {
    console.log(`server is listening ${process.env.HOST}`);
})

module.export = app