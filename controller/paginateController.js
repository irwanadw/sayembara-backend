const db = require("../connection/dbConnection");
const _ = require('lodash')
const humps = require('humps');
const { v4 } = require("uuid");

class paginateController {
    constructor(tablename, jointable, secondjoin) {
        this.tablename = tablename
        this.jointable = jointable
        this.secondjoin = secondjoin
    }

    camelKey(object) {
        return humps.decamelizeKeys(object)
    }

    plainObject(object) {
        return _.toPlainObject(object)
    }

    addNavQuery(query) {

        const newQuery = {}

        for (const key in query) {
            newQuery[`${this.tablename}.${key}`] = query[key]
        }
        return newQuery
    }

    async getPaginateProviderContest(query, search, limit, offset) {
        const results = await db(this.tablename).select('profiles.company_name', 'contests.*', 'categories.category')
            .join(this.jointable, { 'profiles.user_id': 'contests.user_id' })
            .join(this.secondjoin, { 'contests.category_id': 'categories.id' })
            .where(this.addNavQuery(this.camelKey(query))).andWhere('contests.title', 'like', `%${search}%`)
            .orderBy('post_time', 'desc').limit(limit).offset(offset)
            .catch(err => {
                throw err
            })

        return results.map(result => {
            this.plainObject(result)
            return this.camelKey(this.plainObject(result))
        })
    }

    async getPaginateContest(query, search, limit, offset) {
        // const test = db(this.tablename).where(this.camelKey(query)).toQuery()
        // console.log(db(this.tablename).select('profiles.company_name', 'contests.*', 'categories.category')
        //     .join(this.jointable, { 'contests.user_id': 'profiles.user_id' })
        //     .join(this.secondjoin, { 'contests.category_id': 'categories.id' })
        //     .whereIn(`contests.status`, ['Open', 'Close'])
        //     .andWhere(this.addNavQuery(this.camelKey(query)))
        //     .andWhere('contests.title', 'like', `%${search}%`)
        //     .orderBy('post_time', 'desc').limit(limit).offset(offset).toQuery());

        const results = await db(this.tablename).select('profiles.company_name', 'contests.*', 'categories.category')
            .join(this.jointable, { 'contests.user_id': 'profiles.user_id' })
            .join(this.secondjoin, { 'contests.category_id': 'categories.id' })
            .whereIn(`contests.status`, ['Open', 'Close'])
            .andWhere(this.addNavQuery(this.camelKey(query)))
            .andWhere('contests.title', 'like', `%${search}%`)
            .orderBy('post_time', 'desc').limit(limit).offset(offset)
            .catch(err => {
                throw err
            })
        return results.map(result => {
            this.plainObject(result)
            return this.camelKey(this.plainObject(result))
        })
    }
    async getCount(query, search) {
        // const test = db(this.tablename).where(this.camelKey(query)).toQuery()
        const results = await db(this.tablename).count('* as count')
            .join(this.jointable, { 'profiles.user_id': 'contests.user_id' })
            .join(this.secondjoin, { 'contests.category_id': 'categories.id' })
            .where(this.addNavQuery(this.camelKey(query)))
            .andWhere('contests.title', 'like', `%${search}%`)
            .catch(err => {
                throw err
            })
        return results.map(result => {
            this.plainObject(result)
            return this.camelKey(this.plainObject(result))
        })
    }

    async getCountContest(query, search) {
        // const test = db(this.tablename).where(this.camelKey(query)).toQuery()
        const results = await db(this.tablename).count('* as count')
            .join(this.jointable, { 'profiles.user_id': 'contests.user_id' })
            .join(this.secondjoin, { 'contests.category_id': 'categories.id' })
            .where(this.addNavQuery(this.camelKey(query)))
            .andWhere('contests.title', 'like', `%${search}%`)
            .catch(err => {
                throw err
            })
        return results.map(result => {
            this.plainObject(result)
            return this.camelKey(this.plainObject(result))
        })
    }
}

module.exports = paginateController