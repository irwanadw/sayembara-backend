const Jimp = require("jimp");


const ImageKit = require("imagekit");

const { IMAGEKIT_PUBLIC, IMAGEKIT_PRIVATE, IMAGEKIT_ENDPOINT } = process.env
// imagekit configuration
const imagekit = new ImageKit({
    publicKey: IMAGEKIT_PUBLIC,
    privateKey: IMAGEKIT_PRIVATE,
    urlEndpoint: IMAGEKIT_ENDPOINT
});

const watermark = async (ORIGINAL_IMAGE, LOGO, type) => {
    const [image, logo] = await Promise.all([
        Jimp.read(ORIGINAL_IMAGE),
        Jimp.read(LOGO)
    ]);

    logo.resize(image.bitmap.width / 2, Jimp.AUTO);

    const X = image.bitmap.width / 5;
    const Y = image.bitmap.height / 5;

    return await image
        .composite(logo, X, Y, [
            await {
                mode: Jimp.BLEND_SCREEN,
                opacitySource: 0.5,
                opacityDest: 0.5
            }
        ])
        .getBufferAsync(Jimp.MIME_JPEG)
};

module.exports = watermark