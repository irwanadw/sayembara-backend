const db = require("../connection/dbConnection");
const _ = require('lodash')
const humps = require('humps');
const { v4 } = require("uuid");

class cronJobController {
    constructor(tablename) {
        this.tablename = tablename
    }

    camelKey(object) {
        return humps.decamelizeKeys(object)
    }

    plainObject(object) {
        return _.toPlainObject(object)
    }

    async editStatusContest(body) {
        const dateNow = new Date()
        console.log(dateNow);
        const results = await db(this.tablename).update(body).where('status', 'open').andWhere('end_date', '<', dateNow)
            .catch(err => {
                throw err
            })
        // return results // result == 1
        return results
    }
}

module.exports = cronJobController