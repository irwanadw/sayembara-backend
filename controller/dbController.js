const db = require("../connection/dbConnection");
const _ = require('lodash')
const humps = require('humps');
const { v4 } = require("uuid");
const { toInteger } = require("lodash");

class Controller {
    constructor(tablename) {
        this.tablename = tablename
    }

    camelKey(object) {
        return humps.decamelizeKeys(object)
    }

    plainObject(object) {
        return _.toPlainObject(object)
    }

    async get(query) {
        const results = await db(this.tablename).where(this.camelKey(query))
            .catch(err => {
                return err
            })
        return results.map(result => {
            this.plainObject(result)
            return humps.camelizeKeys(this.plainObject(result))
        })
    }

    async add(body) {
        body.id = v4()
        const results = await db(this.tablename).insert(this.camelKey(body))
            .catch(err => {
                throw err
            })
        // return results[0] // result == [0]
        return body
    }

    async edit(id, body) {
        const results = await db(this.tablename).update(this.camelKey(body)).where({ id })
            .catch(err => {
                throw err
            })
        // return results // result == 1
        return body
    }

    async remove(id) {
        const results = await db(this.tablename).delete().where({ id })
            .catch(err => {
                throw err
            })
        return results // result == 1
    }

    async getContestProfile(limit, offset) {
        const result = await db('contests')
            .join("payments", "payments.contest_id", "contests.id")
            .join("users", "users.id", "payments.user_id")
            .join("profiles", "users.id", "profiles.user_id")
            .distinct("contests.id", "contests.title", "contests.status",
                "contests.payment_winner_date", "contests.post_time", "contests.prize",
                "contests.is_winner_paid", "payments.payment_status",
                "profiles.company_name")
            .orderBy("contests.post_time", "desc")
            .limit(limit).offset(offset)
        return result.map(result => {
            this.plainObject(result)
            return this.camelKey(this.plainObject(result))
        })
    }

    async getContestPayment(query) {
        const result = await db('contests')
            .join("users", "users.id", "contests.user_id")
            .leftOuterJoin("payments", "users.id", "payments.user_id")
            .leftOuterJoin("profiles", "users.id", "profiles.user_id")
            .select("contests.id", "contests.title", "contests.status",
                "payments.payment_status", "contests.end_date",
                "contests.prize", "profiles.account_number",
                "contests.announcement_date", "contests.description",
                "profiles.bank", "payments.payment_date",
                "payments.provider_payment_url")
            .where(query)
            .catch(err => {
                throw err
            })
        return result.map(result => {
            this.plainObject(result)
            return this.camelKey(this.plainObject(result))
        })
    }

    async getEmail(body) {
        const result = await db('payments')
            .join("users", "users.id", "payments.user_id")
            .select("users.email")
            .catch(err => {
                throw err
            })
        return result.map(result => {
            this.plainObject(result)
            return this.camelKey(this.plainObject(result))
        })
    }

    async uploadSubmissions(query, body) {
        const results = await db(this.tablename).update(this.camelKey(body)).where(query)
            .catch(err => {
                throw err
            })
        // return results // result == 1
        return body
    }

    async getMyContestSubmission(query, offset, limit) {
        const results = await db(this.tablename)
            .join("contests", "contests.id", "submissions.contest_id")
            .join("users", "contests.user_id", "users.id")
            .join("profiles", "users.id", "profiles.user_id")
            .select(
                "contests.title", "profiles.company_name",
                "contests.prize", "contests.description",
                "contests.post_time", "contests.end_date",
                "contests.announcement_date", "contests.slug",
                "contests.winner_name", "submissions.is_winner",
                "contests.status", "contests.is_winner_paid",
                "profiles.bank", "profiles.firstname", "profiles.lastname"
            )
            .limit(toInteger(limit)).offset(toInteger(offset))
            .where(query)
            .catch(err => {
                throw err
            })
        return results.map(result => {
            this.plainObject(result)
            return this.camelKey(this.plainObject(result))
        })
    }
    async submissionDetail(query, limit, offset) {
        const results = await db(this.tablename)
            .join("contests", "contests.id", "submissions.contest_id")
            .join("users", "contests.user_id", "users.id")
            .join("profiles", "users.id", "profiles.user_id")
            .select(
                "submissions.id", "submissions.first_submitted_poster_url",
                "submissions.second_submitted_poster_url",
                "submissions.third_submitted_poster_url",
            )
            .where(query)
            .limit(limit).offset(offset)
            .catch(err => {
                throw err
            })
        return results.map(result => {
            this.plainObject(result)
            return this.camelKey(this.plainObject(result))
        })
    }

    async winnerBank(contestId) {
        const results = await db("contests")
            .join("submissions", "submissions.contest_id", "contests.id")
            .join("users", "users.id", "submissions.user_id")
            .join("profiles", "profiles.user_id", "users.id")
            .select("profiles.bank", "profiles.account_number",
                "profiles.firstname", "profiles.lastname", "submissions.is_winner")
            .where({ "contests.id": contestId, })
            // .limit(limit).offset(offset)
            .catch(err => {
                throw err
            })
        return results.map(result => {
            this.plainObject(result)
            return this.camelKey(this.plainObject(result))
        })
    }
}

module.exports = Controller