const Controller = require("../controller/dbController");
// const { salt, checkPassword } = require("../helpers/bcryptHelper")
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');
const saltRounds = 1;


const Ajv = require('ajv');
const schema = {
    type: "object",
    properties: {
        email: { type: 'string' },
        password: { type: 'string', minLength: 7, maxLength: 13 },
        company_name: { type: 'string' },
        companyName: { type: 'string' },
        firstname: { type: 'string' },
        lastname: { type: 'string' }
    }
}

var ajv = new Ajv(); // options can be passed, e.g. {allErrors: true}
var validate = ajv.compile(schema);
class User extends Controller {
    constructor(body) {
        super('users');
        this.body = body;
        this.tablename = 'users'
    }

    validate() {
        var valid = validate(this.body);
        if (!valid) throw validate.errors;
    }

    async structureBody() {
        return await this.get(this.body)
            .then(result => {
                return true
            })
            .catch(err => {
                return false
            })
    }

    async hashingAndRegister() {
        this.body.password = await this.salt()
        delete this.body.firstname
        delete this.body.lastname
        delete this.body.company_name
        return await this.add(this.body)
    }

    async login() {
        const result = await this.get({ email: this.body.email })
        if (result.length == 0)
            return false
        else
            return result[0]
    }

    signJwt() {
        this.body.token = jwt.sign(this.body, process.env.JWT_SECRET, { expiresIn: '1d' })
        delete this.body.password
        return this.body
    }

    salt() {
        return new Promise((resolve, reject) => {
            bcrypt.hash(this.body.password, saltRounds, function (err, hash) {
                if (err)
                    reject(err)
                else
                    resolve(hash)
            })
        })
    }

    async checkPassword() {
        let user;
        const result = await this.get({ email: this.body.email })
        user = result[0]
        this.body.id = user.id
        this.body.role = user.role
        return new Promise((resolve, reject) => {
            bcrypt.compare(this.body.password, user.password, function (err, result) {
                if (err)
                    reject(err)
                else
                    resolve(result)
            })
        })
    }

}


module.exports = User
