const Controller = require("../controller/dbController");
const Ajv = require('ajv');
const dayjs = require('dayjs')
const ajv = new Ajv(); // options can be passed, e.g. {allErrors: true}

const contestSchema = {
    type: 'object',
    properties: {
        title: { type: 'string', minLength: 1 },
        prize: { type: 'number', minimum: 500000 },
        description: { type: 'string', minLength: 1 },
        categoryId: { type: 'string', minLength: 1 },
        startDate: { format: 'date' },
        endDate: { format: 'date' },
        announcement_date: { format: 'date' }
    }
}
// console.log(schema);
const validate = ajv.compile(contestSchema);

class Contest extends Controller {
    constructor(body) {
        super('contests')
        this.body = body
    }
    validate() {
        const valid = validate(this.body);
        if (!valid) throw validate.errors;
    }

    validateDate() {
        const startDate = new Date(this.body.startDate)
        if (startDate < dayjs().add(7, 'day')) {
            throw "Start date is not valid"
        }

        if (!(this.body.startDate < this.body.endDate && this.body.endDate < this.body.announcementDate)) {
            throw "Data date is not valid"
        }
    }
    async save() {
        const result = await this.add(this.body)
        return result
    }
}

module.exports = Contest