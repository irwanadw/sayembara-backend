require('dotenv').config()
const express = require('express')
const readdirp = require('readdirp')
const cors = require('cors')
const job = require('./routes/notification/notification')
const app = express()
const jobUpdateContest = require('./helpers/cronJobHelper/cronJobUpdateContest')

// handling CORS issues
const allowedList = [
    "http://localhost",
    "http://localhost:3001",
    "http://localhost:3000"
]
// const corsOptionsDelegate = function (req, callback) {
//     let corsOptions;
//     if (allowedList.indexOf(req.header('Origin')) !== -1) {
//         corsOptions = { origin: true }
//     } else {
//         corsOptions = { origin: false }
//     }
//     callback(null, corsOptions)
// }

// app.use(cors(corsOptionsDelegate))
const corsOptionsDelegate = function (req, callback) {
    callback(null, { origin: true })
}
app.use(cors(corsOptionsDelegate))

app.use(express.json())
// cron job
job.start();
// read all file automatically
readdirp('.', { fileFilter: '*Route.js' })
    .on('data', (entry) => {
        const path = `./${entry.path}`
        const route = require(path)
        app.use(route)
    })

module.exports = app
