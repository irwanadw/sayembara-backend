const request = require('supertest')
const faker = require("faker")

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const { expect } = chai
const { assert } = require('chai')

// import execution script
const execute = require('./executeUtl')

// import router
const getPort = require('get-port')
const app = require('../server')
// import db connection
const db = require("../connection/dbConnection")
let port
before(async () => {
    await execute('npm run db')
    await execute('npm run migrate')

    port = await getPort()

    app.listen(port, () => {
        console.log(`server testing listening on port: ${port}`);
    })
    console.log(`*** DB ADDEDD ***`);
})

after(async () => {
    await execute('npm run db:drop')
    // await db.destroy()
    app.listen().close(() => {
        console.log(`server testing listening to ${port} is closed`);
    })
    console.log(`***DB REMOVED***`);
})