require('dotenv').config()
const { random } = require('faker')
const execute = require('./executeUtl')
const getPort = require('get-port')
const server = require('../server')


class ServerHelper {
  constructor() {
    this.dbName = random.alphaNumeric(10)
  }
  async init() {
    // db setup
    process.env.DB_DATABASE = this.dbName
    await execute('npm run db')
    await execute('npm run migrate')

    // express setup
    const express = require('express')
    this.app = express()
    this.app.use(express.json())

    // route setup
    // const route = require('../routes' + this.routeFileName)
    this.app.use(server)

    // error handler setup
    const ErrorHelper = require('../helpers/errorHelper')
    const errorHelper = new ErrorHelper()
    this.app.use(errorHelper.hander)

    // port setup
    this.port = await getPort()
    this.server = this.app.listen(this.port)
  }
  prep() {
    // controller prep
    const Controller = require('../model/userModel')
    this.controller = new Controller()

    // db prep
    const db = require('../connection/dbConnection')
    this.db = db(this.controller.tableName)
  }
  async drop() {
    // db drop
    process.env.DB_DATABASE = this.dbName
    await execute('npm run db:drop')

    // server drop
    this.server.close()
  }
}

module.exports = ServerHelper