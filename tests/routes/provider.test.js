require('../helper')
// supertest configuration
const request = require('supertest')
// faker configuration
const faker = require("faker")
// chai configuration
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const { expect } = chai
const { assert } = require('chai')

let token;
let body;
// import routers
const server = require('../../server')
// routes initiation
const signUp = '/register'
const signin = '/login'
const dashboard = '/admin/contests'
const contest = '/contests'
const paymentAprv = '/admin/contests/payment'

// data for admin
const user = {
    email: "testting",
    password: "testing123",
    role: "provider",
    company_name: "testing"
}
beforeEach(async () => {
    const result = await request(server)
        .post(signUp)
        .send(user)
    body = result.body
})

describe('____PROVIDER ACCESS', () => {
    it('Provider should be login', async () => {
        const result = await request(server)
            .post(signin)
            .send(
                {
                    email: "testting",
                    password: "testing123",
                }
            )
        expect(result.body).to.have.keys("id", "token", "email", "role")
    })
    it('Provider post contest without data will be empty', async () => {
        const result = await request(server)
            .post(contest)
            .send({
            })
        expect(result.body).empty
    })

    // it('PAYMENT APPROVEMENT PAGE', async () => {
    //     const result = await request(server)
    //         .get(paymentAprv)
    //         .send({
    //             contest_id: "1"
    //         })
    //         .expect(401)
    // })
})
// describe('____PROVIDER ACCESS', () => {
//     it('PROVIDER REGISTERED', async () => {
//         const result = await request(server)
//             .post(signUp)
//             .send({
//                 email: faker.internet.email(12),
//                 password: faker.internet.password(9),
//                 role: "provider",
//                 company_name: faker.name.middleName
//             })
//         tokenProvider = result.body.token
//     })
//     it('SIGN AS PROVIDER', async () => {
//         const result = await request(server)
//             .post(contest)
//             .send({
//                 apa: "salah"
//             })
//             .expect(404)
//     })

// })
