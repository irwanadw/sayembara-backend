require('../helper')
// supertest configuration
const request = require('supertest')
// faker configuration
const faker = require("faker")
// chai configuration
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const { expect } = chai
const { assert } = require('chai')

describe('____ADMIN ACCESS', () => {
    // import routers
    const server = require('../../server')
    // routes initiation
    const signUp = '/register'
    const signin = '/login'
    const dashboard = '/admin/contests'
    const contest = '/contests'
    const paymentAprv = '/admin/contests/payment'

    // data for admin
    const user = {
        email: "admin@gmail.com",
        password: "admin12345",
        role: "admin"
    }
    it('Admin signed up', async () => {
        const result = await request(server)
            .post(signUp)
            .send(user)
        console.log(result.body.role);
    })
})